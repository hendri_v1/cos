<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Merk Management</h3>
    </div>
</div>
<div class="row form-horizontal">
	<div class="col-lg-5">
		<button class="btn btn-primary" id="add-new-display">Add New</button>
	</div>
    <div class="col-lg-7 hidden" id="div-add-new">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	Add New
            </div>
            <div class="panel-body">
            	<form role="form" id="form-add-category">
                	<?php echo $this->mglobal->form_input('category_name','Category Name','text','Enter Name');?>
                    <input type="hidden" name="category_detail" id="category_detail" value="x" />
                    <button class="btn btn-primary" id="add_category"><i class="fa fa-check"></i> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	All Data
            </div>
            <div class="panel-body">
           		<div class="table-responsive">
                	<table class="table table-stripped table-hover table-bordered" id="table-category">
                    	<thead>
                        	<tr>
                            	<th>#ID</th><th>Category Name</th><th>Action</th>
                            </tr>
                            
                        </thead>
                        <tbody>
                        	<?php foreach($query as $rows): ?>
                            	<tr>
                                	<td><?php echo $rows->category_id;?></td>
                                    <td><?php echo $rows->category_name;?></td>
                                    
                                    <td>
                                    	<a href="javascript:void(0);" class="btn btn-primary btn-xs" category_id="<?php echo $rows->category_id;?>">Edit</a>
                                        <a href="javascript:void(0);" class="btn btn-info btn-xs" category_id="<?php echo $rows->category_id;?>">Delete</a>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#table-category').dataTable();
		$('#add-new-display').click(function(){
			$('#div-add-new').removeClass('hidden');
			$('#category_name').focus();
		});
        $('form').on('submit',function(event){
			event.preventDefault();
			var the_data=$(this).serialize();
			$.post('<?php echo site_url('super_admin/ccategory/add_new');?>',the_data,function(data){
				$('#page-wrapper').load('<?php echo site_url('super_admin/ccategory/category_acc');?>');
			});
			
		});
    });
</script>