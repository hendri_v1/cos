<div class="row">
      <div class="col-lg-12">
          <h3 class="page-header">User Management</h1>
      </div>
      <!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-7">
    	<div class="panel panel-default">
        	<div class="panel-heading">All User</div>
        
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#ID</th><th>Username</th><th>Full Name</th><th>Position</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($query as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->user_id;?></td>
                                    <td><?php echo $rows->username;?></td>
                                    <td><?php echo $rows->staff_name;?></td>
                                    <td><?php echo $rows->staff_position_name;?></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-5">
    	<div class="panel panel-default">
        	<div class="panel-heading">Add New</div>
            <div class="panel-body">
            	<form role="form" class="form-horizontal">
                	<?php echo $this->mglobal->form_input('username','Username','text','Input Username');?>
                    <?php echo $this->mglobal->form_input('password','Password','password','Input Password');?>
                    <?php echo $this->mglobal->form_input('staff_name','Full Name','text','Input Full Name');?>
                   	<?php
						$staff_position_id=array();
						foreach($qposition as $rposition)
						{
							$staff_position_id[$rposition->staff_position_id]=$rposition->staff_position_name;	
						}
						echo $this->mglobal->form_dropdown('staff_position_id','Staff Position',$staff_position_id);
					?>
                    <button class="btn btn-primary">Save</button>
                </form>
            </div>	
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('form').on('submit',function(event){
			event.preventDefault();
			the_data=$(this).serialize();
			$.post('<?php echo site_url('super_admin/cuser/add_new');?>',the_data,function(data){
				$('#page-wrapper').load('<?php echo site_url('super_admin/cuser');?>');
			});
		});
    });
</script>