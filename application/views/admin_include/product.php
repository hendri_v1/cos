<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Type Management</h3>
    </div>
</div>
<div class="row form-horizontal">
	<div class="col-lg-12">
    	<button class="btn btn-primary" id="add-new-display">Add New</button>
    </div>
    <hr />
	<div class="col-lg-12 hidden" id="div-add-new">
    	<div class="panel panel-default">
        	<div class="panel-heading">New Product</div>
            <div class="panel-body">
            	<form role="form">
                	<?php echo $this->mglobal->form_input('product_name','Product Name','text','Enter Name');?>
                    <?php echo $this->mglobal->form_input('product_information','Product Information','text','Enter Information');?>
                    <div class="form-group">
                        <label for="category_id" class="col-sm-2">Category</label>
                        <div class="col-sm-10">
                        <?php
                            $category_id=array();
                            foreach($qcategory as $rowscategory)
                            {
                                $category_id[$rowscategory->category_id]=$rowscategory->category_name;	
                            }
                            echo form_dropdown('category_id',$category_id,'','id="category_id" class="form-control"');
                            ?>
                        </div>
                            
                     </div>
                     <?php echo $this->mglobal->form_textarea('product_specification','Product Specification','Input Spec');?>
                        <button class="btn btn-primary" id="add_product"><i class="fa fa-check"></i> Save</button>
					                    
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-8 hidden" id="div-edit-display">
    	
    </div>
</div>
<hr />
<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">All Products</div>
        	<div class="panel-body">
            	<div class="table-responsive">
                	<table class="table table-striped table-hover table-bordered" id="table-product">
                    	<thead>
                        	<tr>
                            	<th>#ID</th><th>Type</th><th>Merk</th><th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php foreach($query as $rows): ?>
                            	<tr>
                                	<td><?php echo $rows->product_id;?></td>
                                    <td><?php echo $rows->product_name;?></td>
                                    <td><?php echo $rows->category_name;?></td>
                                    <td><button class="btn btn-primary btn-xs edit-product" product_id="<?php echo $rows->product_id;?>">Edit</button></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#table-product').dataTable();
		$('#add-new-display').click(function(){
			$('#div-add-new').removeClass('hidden');
		});
		$('#table-product').delegate('.edit-product','click',function(){
			scrollTop: $("#div-edit-display").offset().top
			product_id=$(this).attr('product_id');
			$('#div-edit-display').removeClass('hidden');
			$('#div-edit-display').load('<?php echo site_url('super_admin/cproduct/edit');?>/'+product_id);
		});
        $('form').on('submit',function(event){
			event.preventDefault();
			var error=0;
			var the_data=$(this).serialize();
			if($('#product_name').val()=='')
			{
				error++;
				$('#product_name').focus();
			}
			
			
			if(error==0)
			{
				
				$.post('<?php echo site_url('super_admin/cproduct/add_new');?>',the_data,function(data)
				{
					$('#page-wrapper').load('<?php echo site_url('super_admin/cproduct');?>');
				});
			}
		});
    });
</script>