<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Supplier Management</h3>
    </div>
</div>
<div class="row form-horizontal">
	<div class="col-lg-5">
		<button class="btn btn-primary" id="add-new-display">Add New</button>
	</div>
    <div class="col-lg-12 hidden" id="div-add-new">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	Add New
            </div>
            <div class="panel-body">
            	<form role="form" id="form-add-category">
                	<?php echo $this->mglobal->form_input('supplier_name','Supplier Name','text','Input Name');?>
                    <?php echo $this->mglobal->form_textarea('supplier_address','Address','Input Address');?>
                    <?php echo $this->mglobal->form_input('supplier_phone','Phone','text','Input Phone');?>
                    <?php echo $this->mglobal->form_input('supplier_cp','Contact Person','text','Input Contact Person');?>
                    <?php echo $this->mglobal->form_input('supplier_city','City','text','Input City');?>
                    <?php echo $this->mglobal->form_input('supplier_email','Email','text','Input Email');?>
                    <?php echo $this->mglobal->form_input('supplier_website','Website','text','Input Website');?>
                    <?php echo $this->mglobal->form_input('supplier_account','Rek','text','Input Rek');?>
                    <button id="savenew" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	All Data
            </div>
            <div class="panel-body">
           		<div class="table-responsive">
                	<table class="table table-stripped table-hover table-bordered" id="table-category">
                    	<thead>
                        	<tr>
                            	<th>#ID</th><th>Supplier Name</th><th>Address</th><th>Phone</th><th>Contact Person</th><th>Action</th>
                            </tr>
                            
                        </thead>
                        <tbody>
                        	<?php foreach($query as $rows): ?>
                            	<tr>
                                	<td><?php echo $rows->supplier_id;?></td>
                                    <td><?php echo $rows->supplier_name;?></td>
                                    <td><?php echo $rows->supplier_address;?></td>
                                    <td><?php echo $rows->supplier_phone;?></td>
                                    <td><?php echo $rows->supplier_cp;?></td>
                                    <td><a href="#" class="btn btn-primary btn-xs edit-supplier" supplier_id="<?php echo $rows->supplier_id;?>">Edit</a></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
        $('#add-new-display').click(function(){
			$('#div-add-new').removeClass('hidden');
			
		});
		$('form').on('submit',function(event){
			event.preventDefault();
			the_data=$(this).serialize();
			$.post('<?php echo site_url('super_admin/csupplier/add_new');?>',the_data,function(data)
			{
				$('#page-wrapper').load('<?php echo site_url('super_admin/csupplier');?>');
			});
		});
    });
</script>