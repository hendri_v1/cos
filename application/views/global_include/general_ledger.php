<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">General Ledger</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	New Record
            </div>
            <div class="panel-body" id="report_result">
            	<div class="row form-horizontal">
                	<div class="col-lg-6">
                    	<?php
							$general_ledger_type=array(0=>'Debit',1=>'Credit'); 
							echo $this->mglobal->form_dropdown('general_ledger_type','Tx Type',$general_ledger_type);?>
                        <?php echo $this->mglobal->form_input('general_ledger_title','Title','text','Input Title');?>
                        <?php echo $this->mglobal->form_input('general_ledger_total','Total','text','Input Total');?>
                        <button class="btn btn-info" id="add_gl">Add</button>
                    </div>
                    <div class="col-lg-6">
                    	<table class="table table-stripped">
                    		<tr><td>Last GL Balance</td><td>: </td><td><div align="right"><?php echo 'Rp. '.number_format($this->mgl->get_last_gl(),0,',','.');?></div></td></tr>
                    		<tr><td>Last Sell Balance</td><td>: </td><td><div align="right"><?php echo 'Rp. '.number_format($this->mselling->get_total_sell_today());?></div></td></tr>
                    		<tr><td>Total Balance</td><td>: </td><td><div align="right"><?php echo 'Rp. '.number_format($this->mgl->get_last_gl()+$this->mselling->get_total_sell_today());?></div></td></tr>
                    	</table>
                    	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	Today Records
            </div>
            <div class="panel-body" id="today_result">
            
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#today_result').load('<?php echo site_url('super_admin/creport/generate_gl_today');?>');
		
        $('#add_gl').click(function(){
			var r=confirm("Are you sure ?");
			if(r==true)
			{
				var error=0;
				gl_type=$('#general_ledger_type').val();
				gl_title=$('#general_ledger_title').val();
				gl_total=$('#general_ledger_total').val();
				
				if(gl_title=='')
					error++;
				
				if(gl_total=='')
					error++;
				
				if(error==0)
				{
					$.post('<?php echo site_url('alluser/cmain/add_gl');?>',
					{
						general_ledger_type:gl_type,
						general_ledger_title:gl_title,
						general_ledger_total:gl_total
					},
					function(data)
					{
						alert('ledger added');
						$('#page-wrapper').load('<?php echo site_url('alluser/cmain/general_ledger');?>');
					}
					);
				}
			}
		});
    });
</script>