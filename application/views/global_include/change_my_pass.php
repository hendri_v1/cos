<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Change My Password</h1>
    </div>
</div>

<div class="row">
	<div class="col-lg-4">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	Password
            </div>
            <div class="panel-body">
            	New Password <input type="text" name="new_password" id="new_password" /><br />
                Rep Password <input type="text" name="rep_password" id="rep_password" /><br />
                <button id="change_password">Change</button>
            </div>
        </div>
    
    	
    </div> <!-- col 6 close-->
    <div class="col-lg-8">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	Company Update
            </div>
            <div class="panel-body">
            
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	$(document).ready(function(e) {
		
        $('#change_password').click(function(){
			new_pass=$("#new_password").val();
			rep_pass=$('#rep_password').val();
			if(new_pass!=rep_pass)
			{
				alert('New Password and Repeat Password not same!');
			}
			else
			{
				$.post('<?php echo site_url('program/change_pass');?>',
				{
					new_password:new_pass
				},
				function(data)
				{
					alert('password changed!');	
				});
			}
		});
    });
</script>