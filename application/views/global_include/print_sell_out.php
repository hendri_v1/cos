<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<style>
	table td{font-size:10px;font-family:sans-serif;}
	
	th{font-size:12px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;}
	.row{width:100%;height:450px;}
	.col-lg-6{width:50%;float:left;}
	.col-lg-3{width:25%;float:left;}
	.col-lg-12{width:100%;float:left;}
	li{font-size:9px;}
	#barangnya td{font-size:12px;}
</style>
</head>
<body>
	
    <div class="row">
    	<img src="<?php echo base_url();?>assets/logo.jpg" style="position:absolute;left:0;">
    	<div class="col-lg-3" style="margin-left:100px;">
        	<table>
            	<tr><td>Jl. Moses Gatot Kaca No 1 (Wetan Dewe) Gejayan, Yogyakarta<br />
                	TLP : 085100507776 <br />PIN : 22228448<br /><br />
                  
                </td></tr>
            </table>
        </div>
        <div class="col-lg-3" style="float:right;">
            
            <table class="table" style="width:100%;" cellpadding="0" cellspacing="0">
            	<tr><td colspan="2">&nbsp;</td></tr>
                <tr><td>Tanggal</td><td>: <?php echo mdate('%d-%M-%Y',$row->sell_out_date);?></td></tr>
                
                <tr><td>Nama</td><td>: <?php echo $row->customer_name;?></td></tr>
                <tr><td>Kontak</td><td>: <?php echo $row->customer_phone;?></td></tr>
            </table>
        </div>
        <div class="col-lg-3" style="float:right;">
            <table class="table" style="width:100%;" cellpadding="0" cellspacing="0">
            	<tr><td colspan="2"><strong style="font-size:13px;">Faktur Penjualan</strong></td></tr>
                <tr><td>No Faktur</td><td>: <?php echo $row->sell_out_id;?></td></tr>
                <tr><td>Tipe Pembayaran</td><td>: <?php echo $this->mglobal->sell_out_type($row->sell_out_type);?></td></tr>
                
            </table>
        </div>
        
      <div class="col-lg-12" style="position:absolute;margin-top:100px;">
            <table class="table table-bordered" id="barangnya" style="width:100%;" cellpadding="0" cellspacing="0">
                <thead>
                    <tr><th>No</th><th>Nama Barang</th><th>Harga Satuan</th><th>QTY</th><th></th><th><div align="right">Sub Total</div></th></tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach($dquery as $drows): $i++; ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $drows->category_name.' '.$drows->product_name.' '.$drows->items_name;?> | <?php echo $drows->items_code;?></td>
                            <td><div align="right"><?php echo number_format($drows->detail_sell_out_price,0,',','.');?></div></td>
                            <td><div align="center"><?php echo $drows->qty;?></div></td>
                           	<td>Rp.</td>
                            <td><div align="right"><?php echo number_format($drows->detail_sell_out_total,0,',','.');?></div></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <br />
          <table style="width:100%;" cellpadding="2" cellspacing="0">
           	  <tr>
           	    <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%">Ket :</td>
           	    <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%"><div align="center">Sales</div></td>
                <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%"><div align="center">Diterima Oleh</div></td>
                <td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;" width="25%"><strong><span style="font-size:12px;">Item : <?php echo $i;?> unit</span></strong></td>
              </tr>
               <tr>
               	  <td style="border-left:1px solid #000;"></td>
           	     <td style="border-left:1px solid #000;">&nbsp;</td>
                 <td style="border-left:1px solid #000;"></td>
                 <td style="border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"></td>
              </tr>
              <tr>
               	  <td style="border-left:1px solid #000;"></td>
               	  <td style="border-left:1px solid #000;">&nbsp;</td>
                  <td style="border-left:1px solid #000;"></td>
                  <td style="border-right:1px solid #000;border-left:1px solid #000;"></td>
              </tr>
              <tr>
               	  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>
               	  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"><div align="center"><?php echo $row->username;?></div></td>
                  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"><div align="center"><?php echo $row->customer_name;?></div></td>
                  <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"><strong><span style="font-size:12px;">Total Bayar <?php echo number_format($row->sell_out_total,0,',','.');?></span></strong></td>
              </tr>
              
          </table>
          <p style="font-size:10px;">No Rekening a/n Almansyah, S. Kom : BCA 0372834950, BRI : 098701012858538, MANDIRI : 1370006502054</p>
          	<ol>
            	<li>Garansi Servis ........ hari, non baterai, TC, sparepart dan aksesoris</li>
                <li>Barang yang sudah dibeli, tidak dapat ditukar / dikembalikan</li>
                <li>Klaim / garansi tidak dilayani tanpa nota ini</li>
                
            </ol>
          
        </div>
        
       
    </div>
    
    <?php if(isset($_GET['double'])): ?>
    <div class="row">
    	<img src="<?php echo base_url();?>assets/logo.jpg" style="position:absolute;left:0;">
    	<div class="col-lg-3" style="margin-left:100px;">
        	<table>
            	<tr><td>Jl. Moses Gatot Kaca No 1 (Wetan Dewe) Gejayan, Yogyakarta<br />
                	TLP : 0274-8507776 <br />PIN : 22228448<br /><br />
                    <small>Cab : Jl. Moses Gatot Kaca No. 16, Gejayan, Yogyakarta. T: 0274-7011025</small>
                </td></tr>
            </table>
        </div>
        <div class="col-lg-3" style="float:right;">
            
            <table class="table" style="width:100%;" cellpadding="0" cellspacing="0">
            	<tr><td colspan="2">&nbsp;</td></tr>
                <tr><td>Tanggal</td><td>: <?php echo mdate('%d-%M-%Y',$row->sell_out_date);?></td></tr>
                
                <tr><td>Nama</td><td>: <?php echo $row->customer_name;?></td></tr>
                <tr><td>Kontak</td><td>: <?php echo $row->customer_phone;?></td></tr>
            </table>
        </div>
        <div class="col-lg-3" style="float:right;">
            <table class="table" style="width:100%;" cellpadding="0" cellspacing="0">
            	<tr><td colspan="2"><strong style="font-size:13px;">Faktur Penjualan</strong></td></tr>
                <tr><td>No Faktur</td><td>: <?php echo $row->sell_out_id;?></td></tr>
                <tr><td>Tipe Pembayaran</td><td>: <?php echo $this->mglobal->sell_out_type($row->sell_out_type);?></td></tr>
                
            </table>
        </div>
        
      <div class="col-lg-12" style="position:absolute;margin-top:100px;">
            <table class="table table-bordered" id="barangnya" style="width:100%;" cellpadding="0" cellspacing="0">
                <thead>
                    <tr><th>No</th><th>Nama Barang</th><th>IMEI</th><th></th><th><div align="right">Sub Total</div></th></tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach($dquery as $drows): $i++; ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $drows->category_name.' '.$drows->product_name.' '.$drows->items_name;?></td>
                            <td><?php echo $drows->items_code;?></td>
                           	<td>Rp.</td>
                            <td><div align="right"><?php echo number_format($drows->detail_sell_out_price,0,',','.');?></div></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <br />
          <table style="width:100%;" cellpadding="2" cellspacing="0">
           	  <tr>
           	    <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%">Ket :</td>
           	    <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%"><div align="center">Sales</div></td>
                <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%"><div align="center">Diterima Oleh</div></td>
                <td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;" width="25%"><strong><span style="font-size:12px;">Item : <?php echo $i;?> unit</span></strong></td>
              </tr>
               <tr>
               	  <td style="border-left:1px solid #000;"></td>
           	     <td style="border-left:1px solid #000;">&nbsp;</td>
                 <td style="border-left:1px solid #000;"></td>
                 <td style="border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"></td>
              </tr>
              <tr>
               	  <td style="border-left:1px solid #000;"></td>
               	  <td style="border-left:1px solid #000;">&nbsp;</td>
                  <td style="border-left:1px solid #000;"></td>
                  <td style="border-right:1px solid #000;border-left:1px solid #000;"></td>
              </tr>
              <tr>
               	  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>
               	  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"><div align="center"><?php echo $row->username;?></div></td>
                  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"><div align="center"><?php echo $row->customer_name;?></div></td>
                  <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"><strong><span style="font-size:12px;">Total Bayar <?php echo number_format($row->sell_out_total,0,',','.');?></span></strong></td>
              </tr>
              
          </table>
          
          	<ol>
            	<li>Garansi Servis ........ hari, non baterai, TC, sparepart dan aksesoris</li>
                <li>Barang yang sudah dibeli, tidak dapat ditukar / dikembalikan</li>
                <li>Klaim / garansi tidak dilayani tanpa nota ini</li>
                
            </ol>
          
        </div>
        
       
    </div>
    <?php endif;?>
</body>
</html>
<script type="text/javascript">
	
        window.print();
  
</script>