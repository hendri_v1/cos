<style>
	#tabkhusus li .active{background:none;}
</style>
<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">New Sell Out</h3>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	
            </div>
            <div class="panel-body">
            	<div class="row form-horizontal">
                	<div class="col-lg-6">
                    
                    	<?php echo $this->mglobal->form_input('customer_name','Customer Name','text','Input Name');?>
                        <?php echo $this->mglobal->form_input('customer_phone','Contact','text','Input Contact');?>
                        <?php echo $this->mglobal->form_textarea('customer_address','Address','Input Address');?>
                    </div>
                    <div class="col-lg-6">
                    	<?php echo $this->mglobal->form_input('sell_out_id','No Nota','text','',$new_id);?>
                    	<?php $sell_out_type=array(1=>'Cash',2=>'EDC BCA',3=>'EDC BRI',4=>'EDC MANDIRI',5=>'EDC MEGA',6=>'EDC DANAMON',7=>'TEMPO',8=>'KREDIT');?>
                        <?php $this->mglobal->form_dropdown('sell_out_type','Payment Type',$sell_out_type);?>
                        <?php 
							$user_id=array();
							$queryuser=$this->muser->get_all();
							foreach($queryuser as $rowsuser)
							{
								$user_id[$rowsuser->user_id]=$rowsuser->username;
							}
							echo $this->mglobal->form_dropdown('user_id','Sales',$user_id,$this->session->userdata('user_id'));
						?>
                    </div>
                   
                </div>
                <div class="row">
                	<div class="col-lg-12">
                    	<table class="table table-striped table-bordered">
                        	<thead>
                            	<th>No</th><th>ID</th><th>Name</th><th>Sell Price</th><th>QTY</th><th>Total</th>
                                <tr>
                                	<td></td>
                                    <td><input type="text" id="items_code" name="items_code" style="width:90%;"></td>
                                    <td><span id="items_name"></span><input type="hidden" name="items_id" id="items_id"></td>
                                    <td><input type="text" id="items_base_price" name="items_base_price" width="40%"  value="0" style="color:#FFF;" disabled="disabled" /> <input type="text" id="items_sell_price" name="items_sell_price" style="width:40%;"></td>
                                    
                                    <input type="hidden" id="items_unique" name="items_unique" />
                                    <input type="hidden" id="items_qty" name="items_qty" />
                                    <td><input type="text" id="detail_sell_out_qty" name="detail_sell_out_qty" value="1"></td>
                                    <td><button class="btn btn-primary btn-xs" id="add_items">Add</button></td>
                                </tr>
                            </thead>
                            <tbody id="refresh_detail">
                            	
                                
                            </tbody>
                        </table>
                        <button id="save" class="btn btn-primary">Save</button> <button id="cancel" class="btn btn-info">Cancel</button>
                    </div>
                </div>
            </div>	
            
        </div> 
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		$('#sidebar-menu').hide();
		
		var is_item=0;
		$('#customer_address').val('Yogyakarta');
		$('#sell_out_id').prop("disabled",true);
		$('#items_sell_price').val(0);
		$('#refresh_detail').load('<?php echo site_url('alluser/cmain/refresh_detail/'.$new_id);?>');
        var icodetags = <?php echo $the_items_code;?>;
		$("#items_code").autocomplete({source: icodetags});
		$('#items_code').blur(function(){
			icode=$(this).val();
			$.post('<?php echo site_url('alluser/cmain/get_items');?>',{items_code:icode},function(data)
			{
				$('#items_id').val(data.items_id);
				$('#items_name').html(data.items_name);
				$('#items_base_price').val(data.items_base_price);
				$('#items_sell_price').val(data.items_sell_price);
				$("#items_unique").val(data.items_unique);
				$('#items_qty').val(data.items_qty);
			},'json');
		});
		$('#add_items').click(function(){
			i_id=$('#items_id').val();
			q_ty=$('#detail_sell_out_qty').val();
			so_id=$('#sell_out_id').val();
			i_sell_price=$('#items_sell_price').val();
			i_base_price=$('#items_base_price').val();
			i_unique=$('#items_unique').val();
			i_qty=$('#detail_sell_out_qty').val();
			error=0;
			if(parseInt(i_base_price)>parseInt(i_sell_price))
			{
				alert('Cannot Lower Than Base Price');
				error++;
			}
			else if(i_sell_price === '')
			{
				alert('Please add Sell Price');	
				error++;
			}
			
			if(i_sell_price == '025410019')
			{
				error=0;
				i_sell_price=0;	
			}
			
			if(error==0)
			{
				$.post('<?php echo site_url('alluser/cmain/save_detail_sell_out');?>',
				{
					items_id:i_id,
					qty:q_ty,
					sell_out_id:so_id,
					detail_sell_out_price:i_sell_price,
					items_unique:i_unique,
					items_qty:i_qty
				},
				function(data)
				{
					$('#items_code').val('');
					$('#items_id').val('');
					$('#items_name').html('');
					$('#items_sell_price').val('');
					$('#items_base_price').val('');
					//$('#detail_sell_out_qty').val('');
					$('#refresh_detail').load('<?php echo site_url('alluser/cmain/refresh_detail/'.$new_id);?>');
					is_item++;
				}
				);
			}
		});
		
		$('#detail_sell_out_qty').blur(function(){
			the_value=$(this).val();
			if(the_value==0)
			{
				$(this).val(1);	
			}
			
			if(the_value=='')
			{
				$(this).val(1);	
			}
		});
		
		$('#save').click(function(){
			var r=confirm("Is sales the correct person ?");
			if(r==true)
			{
				var error=0;
				c_name=$('#customer_name').val();
				c_phone=$('#customer_phone').val();
				c_address=$('#customer_address').val();
				so_type=$('#sell_out_type').val();
				so_id=$('#sell_out_id').val();
				so_total=$('#sell_out_total').val();
				u_id=$('#user_id').val();
				
				if(c_name=='')
					error++;
				if(c_phone=='')
					error++;
				if(is_item==0)
					error++;
				if(error==0)
				{
					$.post('<?php echo site_url('alluser/cmain/save_new_sell');?>',
					{
						customer_name:c_name,
						customer_phone:c_phone,
						customer_address:c_address,
						sell_out_type:so_type,
						sell_out_id:so_id,
						sell_out_total:so_total,
						user_id:u_id
					},
					function(data)
					{
						$('#page-wrapper').load('<?php echo site_url('program/main_dashboard');?>');	
						$('#sidebar-menu').show();	
					});
				}
			}
			
		});
		
		$('#cancel').click(function(){
			var r=confirm("Are you sure ?");
			if (r==true)
			{
			 	so_id=$('#sell_out_id').val();
				$.post('<?php echo site_url('alluser/cmain/cancel_sell');?>',{sell_out_id:so_id},function(data){
					$('#page-wrapper').load('<?php echo site_url('program/main_dashboard');?>');
					$('#sidebar-menu').show();
				});
			}
			else
			{
			  	
			}
		});
		window.onbeforeunload = function() {
		  
		  so_id=$('#sell_out_id').val();
			$.post('<?php echo site_url('alluser/cmain/cancel_sell');?>',{sell_out_id:so_id},function(data){
				$('#page-wrapper').load('<?php echo site_url('program/main_dashboard');?>');
				$('#sidebar-menu').show();
			});

		}

		<?php if($this->session->userdata('staff_position_id')>2): ?>
			$('#user_id').attr('disabled', true);
		<?php endif;?>
    });
</script>
