<?php $i=0;$total_all=0;$qty_total=0;; foreach($query as $rows): $i++;?>
	<tr>
    	<td><?php echo $i;?></td>
        <td><?php echo $rows->items_code;?> <a href="javascript:void(0);" class="remove_d_sell_out" detail_sell_out_id="<?php echo $rows->detail_sell_out_id;?>" sell_out_id="<?php echo $rows->sell_out_id;?>" items_id="<?php echo $rows->items_id;?>">| remove</a></td>
        <td><?php echo $rows->category_name.' '.$rows->product_name.' '.$rows->items_name;?></td>
        <td><div align="right"><?php echo number_format($rows->detail_sell_out_price,0,',','.');?></div></td>
        <td><div align="center"><?php echo $rows->qty;?></div></td>
        <td><div align="right"><?php echo number_format($rows->detail_sell_out_total,0,',','.');?></div></td>
    </tr>
	<?php 
		$total_all=$total_all+$rows->detail_sell_out_total;
		$qty_total=$qty_total+$rows->qty;
	?>
<?php endforeach;?>
	<tr>
    	<td colspan="4"><div align="right"><strong>Total</strong></div></td>
        <td><div align="center"><?php echo $qty_total;?></div></td>
        <td><div align="right"><?php echo number_format($total_all,0,',','.');?><input type="hidden" name="sell_out_total" id="sell_out_total" value="<?php echo $total_all;?>"></div></td>
    </tr>
    
<script type="text/javascript">
	$(document).ready(function(e) {
       	$('.remove_d_sell_out').click(function(){
			d_sell_out_id=$(this).attr('detail_sell_out_id');
			s_id=$(this).attr('sell_out_id');
			i_id=$(this).attr('items_id');
			$.post('<?php echo site_url('alluser/cmain/remove_detail_sell_out');?>',
			{
				detail_sell_out_id:d_sell_out_id,
				items_id:i_id
			},
			function(data)
			{
				$('#refresh_detail').load('<?php echo site_url('alluser/cmain/refresh_detail');?>/'+s_id);
			});
		});
    });
</script>