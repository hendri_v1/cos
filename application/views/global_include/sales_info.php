<div class="row">
	<div class="col-lg-6">
        <table class="table">
            <tr><td>Date</td><td><?php echo mdate('%d-%m-%Y %H:%i:%s',$row->sell_out_date);?></td></tr>
            <tr><td>Sales</td>
            	<td>
					<?php echo $row->username;?>
                </td>
            </tr>
            <tr><td>Customer</td><td><?php echo $row->customer_name;?><br /><?php echo $row->customer_address;?><br /><?php echo $row->customer_phone;?></td></tr>
        </table>
    </div>
    <div class="col-lg-6">
    	<table class="table">
        	<tr><td>Nota No</td><td><?php echo $row->sell_out_id;?></td></tr>
        	<tr><td>Payment Type</td><td><?php echo $this->mglobal->sell_out_type($row->sell_out_type);?></td></tr>
            <tr><td>Total</td><td><strong><?php echo number_format($row->sell_out_total,0,',','.');?></strong></td></tr>
        </table>
    </div>
    <div class="col-lg-12">
    	<table class="table table-bordered">
        	<thead>
            	<tr><th>No</th><th>Items</th><th>Price</th><th>QTY</th><th>Sub Total</th></tr>
            </thead>
            <tbody>
            	<?php $i=0; foreach($dquery as $drows): $i++; ?>
                	<tr>
                    	<td><?php echo $i;?></td>
                        <td><?php echo $drows->category_name.' '.$drows->product_name.' '.$drows->items_name;?><br />IMEI <?php echo $drows->items_code;?></td>
                        <td><div align="right"><?php echo number_format($drows->detail_sell_out_price,0,',','.');?></div></td>
                        <td><?php echo $drows->qty;?></td>
                        <td><div align="right"><?php echo number_format($drows->detail_sell_out_total,0,',','.');?></div></td>
                    </tr>
                <?php endforeach;?>
                	<tr>
                    	<td colspan="4"><div align="right">Payment</div></td><td><div align="right"><input type="text" id="payment_text" /></div></td></tr>	
                    <tr><td colspan="4"><div align="right">Return</div></td><td><div align="right"><span id="return" style="font-size:14px;font-weight:bold;"></span></div></td></tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
		Number.prototype.formatMoney = function(c, d, t){
		var n = this, 
			c = isNaN(c = Math.abs(c)) ? 2 : c, 
			d = d == undefined ? "." : d, 
			t = t == undefined ? "," : t, 
			s = n < 0 ? "-" : "", 
			i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
			j = (j = i.length) > 3 ? j % 3 : 0;
		   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		 };
        $("#return").html('<?php echo number_format($drows->detail_sell_out_total,0,',','.');?>');
		$('#payment_text').change(function(){
			if($(this).val()!='')
			{
				nilai=$(this).val();
				pengurang=<?php echo $drows->detail_sell_out_price;?>;
				sisa=nilai-pengurang;
				$('#return').html(sisa.formatMoney(0, ',', '.'));	
			}
		});
		
		
    });
</script>
