<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Unpaid Sales</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">View All | <input type="text" id="search_old" name="search_old" placeholder="Old Nota" /> <input type="text" id="search_by_imei" name="search_by_imei" placeholder="IMEI" /></div>
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="unpaid-table">
                    <thead>
                        <th>No Nota</th><th>Date</th><th>Sales</th><th>Total</th><th>Payment Type</th><th>Action</th>
                    </thead>
                    <tbody>
                    	<?php foreach($query as $rows): ?>
                        	<tr>
                            	<td><?php echo $rows->sell_out_id;?></td>
                                <td><?php echo mdate('%d/%m/%Y %H:%i:%s',$rows->sell_out_date);?></td>
                                <td><?php echo $rows->username;?></td>
                                <td><div align="right"><?php echo number_format($rows->sell_out_total,0,',','.');?></div></td>
                                <td><?php echo $this->mglobal->sell_out_type($rows->sell_out_type);?></td>
                                <td><a href="javascript:void(0);" class="btn btn-primary view-sell btn-xs" sell_out_id="<?php echo $rows->sell_out_id;?>" data-toggle="modal" data-target="#myModal">View</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Sales Detail</h4>
      </div>
      <div class="modal-body" id="unpaid-sales-change">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="print_sell_out" sell_out_id="">Print Double</button> <button type="button" class="btn btn-primary" id="print_sell_out_single">Print Single</button> <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('.view-sell').click(function(){
			sell_out_id=$(this).attr('sell_out_id');
			$('#unpaid-sales-change').load('<?php echo site_url('alluser/cmain/sales_info');?>/'+sell_out_id);
			$('#print_sell_out').attr('sell_out_id',sell_out_id);
			$('#print_sell_out_single').attr('sell_out_id',sell_out_id);
		});
		$('#unpaid-table').dataTable();
		$('#print_sell_out').click(function(){
			sell_out_id=$(this).attr('sell_out_id');
			$('#myModal').modal('hide');
			window.open('<?php echo site_url('alluser/cmain/print_sell_out');?>/'+sell_out_id+'/?double=1');
			//$('#page-wrapper').load('<?php echo site_url('alluser/cmain/unpaid_sales');?>');
			
		});
		
		var icodetags = <?php echo $the_items_code;?>;
		$("#search_by_imei").autocomplete(
			{
				source: icodetags,
				select: function( event, ui ) {
					var search_key=ui.item;
					if(search_key=='')
						$('#search_by_imei').focus();
					else
					{
						$('#myModal').modal('show')
						items_id=search_key.value;
						$('#unpaid-sales-change').load('<?php echo site_url('alluser/cmain/view_items_by_code');?>/'+items_id);
						
					}	
				}
			}
		);
		
		$('#print_sell_out_single').click(function(){
			
			sell_out_id=$(this).attr('sell_out_id');
			$('#myModal').modal('hide');
			window.open('<?php echo site_url('alluser/cmain/print_sell_out');?>/'+sell_out_id);
			//$('#page-wrapper').load('<?php echo site_url('alluser/cmain/unpaid_sales');?>');
			
		});
		
		var json_sell_id = <?php echo $kembali;?>;
		$("#search_old").autocomplete(
			{
				source: json_sell_id,
				select: function( event, ui ) {
					var search_key=ui.item;
					if(search_key=='')
						$('#search_old').focus();
					else
					{
						$('#myModal').modal('show')
						sell_out_id=search_key.value;
						$('#unpaid-sales-change').load('<?php echo site_url('alluser/cmain/sales_info');?>/'+sell_out_id);
						$('#print_sell_out').attr('sell_out_id',sell_out_id);
						$('#print_sell_out_single').attr('sell_out_id',sell_out_id);
					}	
				}
			}
		);
    });
</script>