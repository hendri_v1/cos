<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>COS V.0.9</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/themes/ui-lightness/jquery-ui.css">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">

	<!-- Page-Level Plugin CSS - Tables -->
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">

    <!-- Jquery Chosen -->
    <link href="<?php echo base_url();?>assets/js/chosen/chosen.min.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0" style="">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="<?php echo base_url();?>" style="color:#FFF;margin-top:13px;margin-left:10px;">Sample Demo</a>
            </div>
            <div id="sidebar-search" class="navbar-form navbar-right" style="z-index:9999;">
            
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-left">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $this->session->userdata('staff_name');?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" id="user_proflie"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('program/logout');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation" id="sidebar-menu">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search" id="sidebar-searchx">
                        
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="<?php echo site_url('program');?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                     <?php if($this->session->userdata('staff_position_id')<4): ?>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Accounting<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('alluser/cmain/general_ledger');?>">General Ledger</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('alluser/cmain/unpaid_sales');?>">Unpaid Sales</a></li>
                            <?php if($this->session->userdata('staff_position_id')==1): ?>
                            	<li><a href="javascript:void(0);" goto="<?php echo site_url('alluser/cmain/return_sell');?>">Return Sell</a></li>
                            <?php endif;?>
                            
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Report<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<?php if($this->session->userdata('staff_position_id')==1): ?>
	                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/sales_report');?>">Sales Report</a></li>
                            <?php endif;?>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/unfinished_sales');?>">Unfinished Sales</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/so_report');?>">Sell Out Report</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/sold_out');?>">Sold Out</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/stock_in_report');?>">Stock In Report</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/gl_report');?>">GL Report</a></li>
                            <?php if($this->session->userdata('staff_position_id')==1): ?>
                            	<li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_control');?>">Stock Control HP &amp; Modem</a></li>
                            	<li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/retur_list');?>">Retur List</a></li>
                                <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/creport/in_supplier');?>">Return to Supplier List</a></li>
                            <?php endif;?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    
                    <?php endif;?>
                   
                    <li>
                    	<a href="#"><i class="fa fa-exchange fa-fw"></i> Stock Opname<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_opname');?>">Stock Opname HP New</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_opname2');?>">Stock Opname HP 2nd</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_opname4');?>">Stock Opname Modem</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_opname3');?>">Stock Opname Acc</a></li>
                        </ul>
                    </li>
                    <?php if($this->session->userdata('staff_position_id')<=2): ?>
                    
                    <li>
                    	<a href="#"><i class="fa fa-exchange fa-fw"></i> Stock Management<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/cactivity/stock_in/hp');?>">Handphone In</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/cactivity/stock_in/modem');?>">Modem In</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/cactivity/stock_in_acc');?>">Accesorries In</a>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation');?>">Stock Location</a></li>
                            
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_control_acc');?>">Stock Control Acc</a></li>
                            
                            
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('warehouse/clocation/stock_management');?>">Product List</a></li>
                        </ul>
                    </li>
                   
                    <li>
                        <a href="#"><i class="fa fa-table fa-fw"></i> Master Data Handphone<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/ccategory');?>">Merk</a></li>
                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/cproduct');?>">Type</a></li>
                            <?php if($this->session->userdata('staff_position_id')==1): ?>
	                            <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/csupplier');?>">Supplier</a></li>
                      
                            
    	                        <li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/cuser');?>">User Account</a></li>
                            <?php endif;?>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="#"><i class="fa fa-table fa-fw"></i> Master Data Accessories<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                        	<li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/ccategory/category_acc');?>">Category</a></li>
                      		<li><a href="javascript:void(0);" goto="<?php echo site_url('super_admin/cproduct/type_acc');?>">Type</a></li>
                            
                           
                        </ul>
                    </li>
					<?php endif;?>
                    
                    <li>
                    	<a href="javascript:void(0);"><i class="fa fa-dollar fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                       		<li><a href="javascript:void(0);" goto="<?php echo site_url('alluser/cmain/new_sell_out');?>">New Sell Out</a></li>
                            
                        </ul>
                    </li>
                    <?php if($this->session->userdata('staff_position_id')==1): ?>
                     <li>
                        <a href="<?php echo site_url('program/backup_db');?>"><i class="fa fa-dashboard fa-fw"></i> Backup DB</a>
                    </li>
                    <?php endif;?>
                    
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper"> <!-- content start here -->
                       
        </div> <!-- content end here -->
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
    
    
    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    
    <!-- jquery -->
    <script src="<?php echo base_url();?>assets/js/ui/jquery-ui.custom.js"></script>
	<script src="<?php echo base_url();?>assets/js/ui/jquery.ui.core.js"></script>
    <script src="<?php echo base_url();?>assets/js/ui/jquery.ui.widget.js"></script>
    <script src="<?php echo base_url();?>assets/js/ui/jquery.ui.menu.js"></script>
    <script src="<?php echo base_url();?>assets/js/ui/jquery.ui.position.js"></script>
    <script src="<?php echo base_url();?>assets/js/ui/jquery.ui.datepicker.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/typeahead.bundle.js"></script>

    <!-- Jquery Choose -->
    <script src="<?php echo base_url();?>assets/js/chosen/chosen.jquery.min.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference 
    <script src="<?php echo base_url();?>assets/js/demo/dashboard-demo.js"></script>-->
	<script type="text/javascript">
		$(document).ready(function(e) {
			
			$('#sidebar-search').load('<?php echo site_url('program/global_search');?>');
			$('#page-wrapper').load('<?php echo site_url('program/main_dashboard');?>');
            $('#side-menu li.active ul li a').click(function(){
				$('#side-menu li ul li').removeClass('active');
				$(this).parent().addClass('active');
				
			});
			
			$('#side-menu li a').click(function(){
				$('#side-menu li').removeClass('active');
				$(this).parent().addClass('active');
				idnya=$(this).attr('goto');
				$('#page-wrapper').load(idnya);
			});
			
			$('#user_proflie').click(function(){
				$('#page-wrapper').load('<?php echo site_url('program/change_my_password');?>');
			});
			
			
        });
	</script>
</body>

</html>
