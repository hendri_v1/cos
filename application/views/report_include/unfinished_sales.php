<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Unfinished Sales</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	List Of Invoices
            </div>
            <div class="panel-body" id="report_result">
            	<table class="table table-striped">
					<thead>
						<tr>
							<th>No.</th><th>Invoice No.</th><th>Item</th><th>IMEI</th><th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($query->result() as $rows): ?>
							<tr>
								<td></td>
								<td><?php echo $rows->lock_nota_id;?></td>
								<td><?php echo $rows->product_name;?> <?php echo $rows->items_name;?></td>
								<td><?php echo $rows->items_code;?></td>
								<td></td>
							</tr>
						<?php endforeach;?>
					</tbody>

            	</table>
            </div>
        </div>
    </div>
</div>