<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Sell Out Report</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	<input type="text" id="start_date" name="start_date" placeholder="Start" value="<?php echo date('Y-m-d');?>" /> <input type="text" id="end_date" name="end_date" placeholder="End" value="<?php echo date('Y-m-d');?>" /> <button class="btn btn-default btn-xs" id="generate_report">View</button>
            </div>
            <div class="panel-body" id="report_result">
            
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
        $('#start_date').datepicker(
			{ dateFormat: "yy-mm-dd" }
		);
		$('#end_date').datepicker(
			{ dateFormat: "yy-mm-dd" }
		);
		
		$('#generate_report').click(function(){
			s_date=$('#start_date').val();
			e_date=$('#end_date').val();
			$.post('<?php echo site_url('super_admin/creport/generate_so_report');?>',
				{
					start_date:s_date,
					end_date:e_date
				},
				function(data){
					$('#report_result').html(data);
				}
			);
		});
    });
</script>