
<table class="table table-striped table-bordered">
	<thead>
    	<tr><th>#</th><th>Date</th><th>Tx Title</th><th>Detail</th><th>Debit</th><th>Credit</th><th>Balance</th><th>User</th></tr>
    </thead>
    <tbody>
    	<?php $i=0; foreach($query as $rows): $i++;?>
        	<tr>
            	<td><?php echo $i;?></td>
                <td><?php echo mdate('%d/%m/%Y %H:%i:%s',$rows->general_ledger_date);?></td>
                <td>
					<?php 
						
						if($rows->general_ledger_ref<>"none")
						{
							$row=$this->mselling->get_detail_by_dsell_out_id($rows->general_ledger_ref);
							echo "&nbsp;";
							if($row=="empty")
								echo "Cancelled Nota";
							else
							{
								echo $row->sell_out_id;
							}
						}
						else
						{
							echo "-";	
						}
						
					?>
                </td>
                <td>
                	<?php
					if($rows->general_ledger_ref<>"none")
						{
							$row=$this->mselling->get_detail_by_dsell_out_id($rows->general_ledger_ref);
							echo "&nbsp;";
							if($row=="empty")
								echo '<font color="red">'.$rows->general_ledger_title.'</font>';
							else
							{
								echo $row->category_name.' '.$row->product_name.' '.$row->items_name.' | '.$row->items_code;
							}
						}
						else
						{
							echo $rows->general_ledger_title;	
						}
					?>
                </td>
                <?php if($rows->general_ledger_type==0): ?>
                	<td><div align="right"><?php echo number_format($rows->general_ledger_total,0,',','.');?></div></td>
                    <td></td>
                <?php else: ?>
                	<td></td>
                    <td><div align="right"><?php echo number_format($rows->general_ledger_total,0,',','.');?></div></td>
                    
                <?php endif;?>
                <td><div align="right"><?php echo number_format($rows->general_ledger_balance,0,',','.');?></div></td>
                <td><?php echo $rows->username;?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
<hr />
<a href="javascript:void(0);" id="send_this_page" class="btn btn-sm btn-primary">Backup Report</a>
<script type="text/javascript">
	$(document).ready(function(){
		$('#send_this_page').click(function(){
			s_date=<?php echo $start_date;?>;
			e_date=<?php echo $end_date;?>;
			$.post('<?php echo site_url('super_admin/creport/send_gl_email');?>',
			{
				start_date:s_date,
				end_date:e_date
			},
			function(data)
			{
				alert('Report Backup Done!!!');
			});
		})
	})
</script>
