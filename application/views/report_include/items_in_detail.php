<table class="table">
	<thead>
		<tr>
			<th>#</th><th>Items</th><th>Total</th><th>Price/Unit</th><th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php $total_items=0; $total_price=0; $i=0; foreach($query as $rows): $i++; ?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $rows->category_name.' '.$rows->product_name;?></td>
				<td><?php echo $rows->total_product;?></td>
				<td><div align="right"><?php echo number_format($rows->items_base_price,0,',','.');?></div></td>
                <td><div align="right"><?php echo number_format($rows->items_base_price*$rows->total_product);?></div></td>
			</tr>
		<?php $total_items=$total_items+$rows->total_product; $total_price=$total_price+($rows->items_base_price*$rows->total_product); endforeach;?>
        	<tr>
            	<td colspan="2">Total</td>
                <td><?php echo $total_items;?></td>
                <td></td>
                <td><div align="right"><strong><?php echo number_format($total_price,0,',','.');?></strong></div></td>
            </tr>
	</tbody>
</table>