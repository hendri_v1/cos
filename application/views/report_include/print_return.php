<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<style>
	table td{font-size:10px;font-family:sans-serif;}
	
	th{font-size:12px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000;}
	.row{width:100%;height:450px;}
	.col-lg-6{width:50%;float:left;}
	.col-lg-3{width:25%;float:left;}
	.col-lg-12{width:100%;float:left;}
	li{font-size:9px;}
	#barangnya td{font-size:12px;}
</style>
</head>
<body>
	
    <div class="row">
    	<img src="<?php echo base_url();?>assets/logo.jpg" style="position:absolute;left:0;">
    	<div class="col-lg-3" style="margin-left:100px;">
        	<table>
            	<tr><td>Jl. Moses Gatot Kaca No 1 (Wetan Dewe) Gejayan, Yogyakarta<br />
                	TLP : 0274-8507776 <br />PIN : 22228448<br /><br />
                    <small>Cab : Jl. Moses Gatot Kaca No. 16, Gejayan, Yogyakarta. T: 0274-7011025</small>
                </td></tr>
            </table>
        </div>
        <div class="col-lg-3" style="float:right;">
            
            <table class="table" style="width:100%;" cellpadding="0" cellspacing="0">
            	<tr><td colspan="2">&nbsp;</td></tr>
                <tr><td>Tanggal</td><td>: <?php echo date('d/m/Y');?></td></tr>
                
                <tr><td>Kepada</td><td>: <?php echo $row->supplier_name;?></td></tr>
               
            </table>
        </div>
        <div class="col-lg-3" style="float:right;">
            <table class="table" style="width:100%;" cellpadding="0" cellspacing="0">
            	<tr><td colspan="2"><strong style="font-size:13px;">Nota Retur</strong></td></tr>
                <tr><td>No Nota</td><td>: <?php echo $row->items_source_ref;?></td></tr>
                
                
            </table>
        </div>
        
      <div class="col-lg-12" style="position:absolute;margin-top:100px;">
            <table class="table table-bordered" id="barangnya" style="width:100%;" cellpadding="0" cellspacing="0">
                <thead>
                    <tr><th>#</th><th>Nama Barang</th><th>IMEI</th></tr>
                </thead>
                <tbody>
                   
                        <tr>
                            <td>1</td>
                            <td><?php echo $row->category_name.' '.$row->product_name.' '.$row->items_name;?></td>
                            <td><?php echo $row->items_code;?></td>
                           	
                        </tr>
                   
                </tbody>
            </table>
            <br />
          <table style="width:100%;" cellpadding="2" cellspacing="0">
           	  <tr>
           	    <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%">Ket :
                	<br />
                    <?php $this->db->limit(1);
						$this->db->where('items_id',$row->items_id);
						$query=$this->db->get('retur_record');
						$row=$query->row();
						echo $row->retur_note;
					?>
                </td>
           	    <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%"><div align="center">Raflesia Cell</div></td>
                <td style="border-top:1px solid #000;border-left:1px solid #000;" width="25%"><div align="center">Diterima Oleh</div></td>
                <td style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;" width="25%"><strong><span style="font-size:12px;">Item : 1 unit</span></strong></td>
              </tr>
               <tr>
               	  <td style="border-left:1px solid #000;"></td>
           	     <td style="border-left:1px solid #000;">&nbsp;</td>
                 <td style="border-left:1px solid #000;"></td>
                 <td style="border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;"></td>
              </tr>
              <tr>
               	  <td style="border-left:1px solid #000;"></td>
               	  <td style="border-left:1px solid #000;">&nbsp;</td>
                  <td style="border-left:1px solid #000;"></td>
                  <td style="border-right:1px solid #000;border-left:1px solid #000;"></td>
              </tr>
              <tr>
               	  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"></td>
               	  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"><div align="center"></div></td>
                  <td style="border-bottom:1px solid #000;border-left:1px solid #000;"><div align="center"></div></td>
                  <td style="border-bottom:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;"></td>
              </tr>
              
          </table>
          
          	
          
        </div>
        
       
    </div>
    
</body>
</html>
<script type="text/javascript">
	
        window.print();
  
</script>