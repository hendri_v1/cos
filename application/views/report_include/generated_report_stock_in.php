<table class="table">
	<thead>
		<tr>
			<th>#</th><th>Supplier</th><th>Nota No</th><th>Date</th><th>Total</th>
		</tr>
	</thead>
	<tbody>
    	<tr>
        	<td></td>
            <td>
            	<select name="supplier_select" id="supplier_select">
                	<?php
						$x=0;
						$supplier_id=array();
					 $the_query=$query;
					 if(isset($query2))
					 	$the_query=$query2;
					 foreach($the_query as $rows):$x++;
					 $supplier_id[$x]=$rows->supplier_id;
					 if($x>1) :
					 ?>
						 <?php if($supplier_id[$x]<>$supplier_id[$x-1]): ?>
					       	<option value="<?php echo $rows->supplier_id;?>"<?php if($supplier_idx==$rows->supplier_id) echo ' selected="selected"';?>><?php echo $rows->supplier_name;?></option>
                         <?php endif;?>
                     <?php else: ?>
                     	<option value="<?php echo $rows->supplier_id;?>"<?php if($supplier_idx==$rows->supplier_id) echo ' selected="selected"';?>><?php echo $rows->supplier_name;?></option>
                     <?php endif;?>
                    <?php endforeach;?>
            	</select> <button class="btn btn-info btn-xs" id="filter_supplier">Go</button>  
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
		<?php $i=0; foreach($query as $rows): $i++; ?>
			<tr>
				<td><?php echo $i;?></td>
				<td><a href="#" class="detail_items_in" i-source-ref="<?php echo $rows->items_source_ref;?>" data-toggle="modal" data-target="#myModal"><?php echo $rows->supplier_name;?></a></td>
				<td><?php echo $rows->items_source_ref;?></td>
				<td><?php echo mdate('%d %M %Y',$rows->items_date_in);?></td>
				<td><?php echo $rows->total_items;?></td>
				
			</tr>
		<?php endforeach;?>
	</tbody>
</table>

<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Items In Detail</h4>
      </div>
      <div class="modal-body" id="unpaid-sales-change">
        
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.detail_items_in').click(function(){
			i_source_ref=$(this).attr('i-source-ref');
			$.post('<?php echo site_url('super_admin/creport/detail_items_in');?>',
			{
				items_source_ref:i_source_ref
			},
			function(data)
			{
				$('#unpaid-sales-change').html(data);
			}
			);
		});
		
		$('#filter_supplier').click(function(){
			s_date=$('#start_date').val();
            e_date=$('#end_date').val();
            $.post('<?php echo site_url('super_admin/creport/generate_stock_in_report');?>/'+$('#supplier_select').val(),
                {
                    start_date:s_date,
                    end_date:e_date
                },
                function(data){
                    $('#report_result').html(data);
                }
            );
		});
	})
</script>