<table class="table">
	<thead>
		<tr>
			<th>#</th><th>No Nota</th><th>Product</th><th>IMEI</th><th>Base Price</th><th>Sell Price</th><th>Profit</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$total_profit=0;
			$i=0; foreach($query as $rows): $i++; ?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $rows->sell_out_id;?></td>
				<td><?php echo $rows->product_name.' '.$rows->items_name;?></td>
				<td><?php echo $rows->items_code;?></td>
				<td><div align="right"><?php echo number_format($rows->items_base_price,0,',','.');?></div></td>
				<td><div align="right"><?php echo number_format($rows->detail_sell_out_price,0,',','.');?></div></td>
				<td><div align="right"><?php $total_profit=$total_profit+($rows->detail_sell_out_price-$rows->items_base_price); echo number_format($rows->detail_sell_out_price-$rows->items_base_price,0,',','.');?></div></td>

			</tr>
		<?php endforeach;?>
		<tr>
			<td colspan="7"><div align="right"><?php echo number_format($total_profit,0,',','.');?></div></td>
		</tr>
	</tbody>
</table>