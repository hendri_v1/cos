<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">Product List</h3>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Filter</div>
            <div class="panel-body">
            	<div class="row">
            		<div class="col-sm-4">
            			<?php
            				$product_status=array(0=>"HP New",1=>"HP Second",2=>"All", 3=>"Modem");
            				echo $this->mglobal->form_dropdown('product_status','Type',$product_status); 
            			?>
            		</div>
            		<div class="col-sm-4">
            			<?php
                          $qproduct=$this->mmasterdata->get_all_category();
                          $product_id=array();
						  $product_id[0]="All";
                          foreach($qproduct as $rproduct)
                          {
                              $product_id[$rproduct->category_id]=$rproduct->category_name;	
                          }
                          echo $this->mglobal->form_dropdown('product_id','Merek',$product_id); 
						?>
            		</div>
            		<div class="col-sm-4">
            			<button id="generate" class="btn btn-sm btn-primary">Generate List</button>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">Result</div>
            <div class="panel-body" id="generate_result">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#generate').click(function(){
			$('#generate_result').html('Loading....');
			p_status=$('#product_status').val();
			p_id=$('#product_id').val();
			$.post('<?php echo site_url('warehouse/clocation/generate_list_result');?>',
			{
				is_second:p_status,
				product_id:p_id
			},
			function(data)
			{
				
				
				$('#generate_result').html(data);
			}
			);
		})
	});
</script>