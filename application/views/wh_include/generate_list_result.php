<table class="table table-striped">
	<thead>
		<tr><th>No.</th><th>Merek</th><th>Type</th><th>Total</th><th>Harga</th></tr>
	</thead>
	<tbody>
		<?php $i=0; foreach($query->result() as $rows): $i++; ?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $rows->category_name;?></td>
				<td><?php echo $rows->product_name;?> </td>
				<td><?php echo $rows->total_items;?></td>
				<td><div align="right"><?php echo number_format($rows->items_base_price,0,',','.');?></div></td>
			</tr>
		<?php endforeach;?>
	</tbody>
</table>