<div class="row">
	<div class="col-lg-12">
    	<h3 class="page-header">
		<?php if($ada==0): ?>
        	Data Not Found! <span class="badge pull-right" style="font-size:38px;background:red;"><?php echo $totalnya;?></span>
        <?php else: ?>
			<?php echo $row->category_name.' '.$row->product_name.' '.$row->items_name.' | '.$row->items_code;?> <span class="badge pull-right" style="font-size:38px;background:red;"><?php echo $totalnya;?></span></h3>
        <?php endif;?>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
    	<button id="finish_check" class="btn btn-primary">Finish</button> <button id="print_opname" class="btn btn-primary"><i class="fa-table fa"></i> Export to XLS</button>
        <hr />
    	<table class="table table-striped table-bordered">
        	<thead>
            	<th>No</th><th>Date</th><th>Time</th><th>Items</th><th>IMEI</th><th>Status</th><th>Staff</th>
            </thead>
            <tbody>
            	<?php $i=0; foreach($query as $rows): $i++;?>
                	<tr>
                    	<td><?php echo $i;?></td>
                        <td><?php echo mdate('%d/%m/%Y',$rows->stock_check_date);?></td>
                        <td><?php echo mdate('%h:%i:%s',$rows->stock_check_date);?></td>
                        <td><?php echo $rows->product_name.' '.$rows->items_name;?></td>
                        <td><?php echo $rows->items_code;?></td>
                        <td><?php if($rows->is_second==0)
									echo "New";
								  else
								  	echo "2nd";
							?>
                        </td>
                        <td><?php echo $rows->username;?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        
    </div>
    
    <div class="col-lg-12" id="not_found">
    		
    </div> 
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#finish_check').click(function(){
			
        	$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/not_found/'.$is_second.'/'.$is_modem);?>');
		});
		
		$('#print_opname').click(function(){
			window.open('<?php echo site_url('warehouse/clocation/print_stock_opname/'.$is_second.'/'.$is_modem);?>');
		});
    });
</script>

