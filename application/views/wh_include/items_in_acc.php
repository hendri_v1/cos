<ol class="breadcrumb">
    	<li><a href="javascript:void(0);">Stock Control System</a></li>
        <li><a href="javascript:void(0);"><?php echo $rlocation->location_name;?></a></li>
        
    </ol>

<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading"><?php echo $rlocation->location_name;?></div>
    		<div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-items-in">
                        <thead>
                            <tr>
                                <th>No.</th><th>Category</th><th>Type</th><th>Items Type</th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0; foreach($query as $rows): $i++; ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $rows->category_name;?></td>
                                    <td><?php echo $rows->product_name;?></td>
                                    <td><?php echo $rows->total_items;?></td>
                                    <td>
                                    	<button class="btn btn-primary btn-xs items-detail" product_id="<?php echo $rows->product_id;?>">View All</button>
                                    	
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
             </div>
         </div>
    </div>
</div>



<script type="text/javascript">
	$(document).ready(function(e) {
		$('#table-items-in .items-detail').click(function(){
			
			the_id=$(this).attr('product_id');
			$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in_product_acc');?>/'+the_id+'/<?php echo $rlocation->location_id;?>');
		});
		$('#table-items-in').dataTable();
        
		
    });
</script>