<ol class="breadcrumb">
        <li><a href="#">Stock Control System</a></li>
        
    </ol>
<div class="row">
	<div class="col-lg-4">
    	<button class="btn btn-primary" id="add-new-show">Add</button>
    </div>
	<div class="col-lg-8">
   		<div class="panel panel-default hidden" id="div-search-tool">
        	<div class="panel-heading">Search Tool</div>
       	</div>
    </div>
</div>
<hr />
<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-default">
        	<div class="panel-heading">All Locations</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-stock-control">
                        <thead>
                            <tr>
                                <th>#ID</th><th>Location Name</th><th>Info</th><th>Total Items</th><th>Total Assets</th><th>Last Activity</th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($query as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->location_id;?></td>
                                    <td><?php echo $rows->location_name;?></td>
                                    <td><?php echo $rows->location_info;?></td>
                                    <td><div align="center"><?php echo $this->mstock->get_total_by_location($rows->location_id,1);?></div></td>
                                    <td><div align="right"><?php echo number_format($this->mstock->get_assets_by_location($rows->location_id,1),0,',','.');?></div></td>
                                    <td><?php echo mdate('%d/%m/%Y %h:%i:%s',$rows->location_last_log);?></td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-primary btn-xs view-items-loc" loc_id="<?php echo $rows->location_id;?>">View Items</a>
                                        <button class="btn btn-info btn-xs" loc_id="<?php echo $rows->location_id;?>" data-toggle="modal" data-target="#myModal">Activity Log</button>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Activity Log</h4>
      </div>
      <div class="modal-body" id="activity_log_changes">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#add-new-show').click(function(){
			$('#div-search-tool').removeClass('hidden');
		});
		$('#table-stock-control').dataTable();
        $('.view-items-loc').click(function(){
			the_id=$(this).attr('loc_id');
			$('#page-wrapper').load('<?php echo site_url('warehouse/clocation/items_in_acc');?>/'+the_id);
		});
    });
</script>