<div class="panel panel-default">
	<div class="panel-heading">Items Detail</div>
	<div class="panel-body">
    	<div class="row">
    		<div class="col-md-6">
            	Items ID<br />
                Imei<br />
                Name<br />
                Location<br />
                Merk<br />
                Input Date<br />
                Total Aging
            </div>
            <div class="col-md-6">
            	<?php echo $row->items_id;?><br />
                <?php echo $row->items_code;?><br />
                <?php echo $row->items_name;?><br />
                <?php echo $row->location_name;?><br />
                <?php echo $row->product_name;?><br />
                <?php echo mdate('%d/%m/%Y %h:%i:%s',$row->items_date_in);?><br />
                <strong><?php echo timespan($row->items_date_in,time());?></strong>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Pricing and Source</div>
    <div class="panel-body">
    	<p>
    	Base Price is <strong><?php echo number_format($row->items_base_price,0,',','.');?></strong><br />
        Sell Price is <strong><?php echo number_format($row->items_sell_price,0,',','.');?></strong><br />
        Margin is <strong><?php echo number_format($row->items_sell_price-$row->items_base_price,0,',','.');?></strong>
        </p>
        <p>
        	Item came from <strong><?php echo $row->items_source;?></strong> with refference number <strong><?php echo $row->items_source_ref;?></strong>
        </p>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Detail and Condition</div>
    <div class="panel-body">
    	<p>
        	<?php echo $row->product_specification;?>
        </p>
        <p>
        	<?php echo $row->items_condition;?>
        </p>
    </div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Item Activity</div>
    <div class="panel-body">
    	<table class="table table-striped">
        	<thead>
            	<tr>
                	<th>No</th><th>Date</th><th>User</th><th>Activity</th>
                </tr>
            </thead>
            <tbody>
            	<?php $i=0; foreach($qact as $ract): $i++; ?>
                	<tr>
                    	<td><?php echo $i;?></td>
                        <td><?php echo mdate('%d/%m/%Y %h:%i:%s',$ract->stock_activity_date);?></td>
                        
                        <td><?php echo $ract->username;?></td>
                        <td><?php echo $ract->stock_activity_log;?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>