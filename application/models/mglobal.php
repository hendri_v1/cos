<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mglobal extends CI_Model {
	
	function form_input($the_id,$the_label,$the_type,$the_text,$the_value='')
	{
		echo '<div class="form-group">';
    	echo '<label for="'.$the_id.'" class="col-sm-2">'.$the_label.'</label>';
		echo '<div class="col-sm-10">';
    	echo '<input name="'.$the_id.'" type="'.$the_type.'" class="form-control input-sm" id="'.$the_id.'" placeholder="'.$the_text.'" value="'.$the_value.'">';
		echo '</div>';
  		echo '</div>';
	}
	
	function form_textarea($the_id,$the_label,$the_text,$the_value='')
	{
		echo '<div class="form-group">';
    	echo '<label for="'.$the_id.'" class="col-sm-2">'.$the_label.'</label>';
		echo '<div class="col-sm-10">';
    	echo '<textarea name="'.$the_id.'" class="form-control input-sm" id="'.$the_id.'" placeholder="'.$the_text.'">'.$the_value.'</textarea>';
  		echo '</div>';
		echo '</div>';
	}
	
	function form_dropdown($the_id,$the_label,$thearray,$default="no")
	{
		echo '<div class="form-group">';
		echo '<label for="'.$the_id.'" class="col-sm-2">'.$the_label.'</label>';
		echo '<div class="col-sm-10">';
		if($default=="no")
			echo form_dropdown($the_id,$thearray,'','id="'.$the_id.'" class="form-control input-sm"');
		else
			echo form_dropdown($the_id,$thearray,$default,'id="'.$the_id.'" class="form-control input-sm"');
		echo '</div>';	
		echo '</div>';
	}
	
	function activity_type($stock_activity_type)
	{
		if($stock_activity_type==0)
			echo "Out from Location";
		elseif($stock_activity_type==1)
			echo "In to Location";
		elseif($stock_activity_type==2)
			echo "Sell Out";
				
	}
	
	function product_status($product_status)
	{
		if($product_status==0)
			echo "New";
		else
			echo "Second";
	}
	
	function sell_out_type($sell_out_type)
	{
		$type=array(1=>'Cash',2=>'EDC BCA',3=>'EDC BRI',4=>'EDC MANDIRI',5=>'EDC MEGA',6=>'EDC DANAMON',7=>'TEMPO',8=>'KREDIT');	
		return $type[$sell_out_type];
	}
	
}