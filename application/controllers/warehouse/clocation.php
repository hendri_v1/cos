<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clocation extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			redirect('program');
    }
	
	function index()
	{
		$data['query']=$this->mstock->get_location();
		$this->load->view('wh_include/location_management',$data);	
	}
	
	function add_new()
	{
		$this->mstock->location_name=$this->input->post('location_name');
		$this->mstock->location_info=$this->input->post('location_info');
		$this->mstock->add_new_location();	
	}
	
	function stock_control()
	{
		$data['query']=$this->mstock->get_location();
		$this->load->view('wh_include/stock_control',$data);
	}
	
	function stock_control_acc()
	{
		$data['query']=$this->mstock->get_location();
		$this->load->view('wh_include/stock_control_acc',$data);
	}
	
	function items_in($location_id)
	{
		$data['rlocation']=$this->mstock->get_location_by_id($location_id);
		$data['query']=$this->mstock->get_items_by_location($location_id);
		$this->load->view('wh_include/items_in',$data);	
	}
	
	function items_in_acc($location_id)
	{
		$data['rlocation']=$this->mstock->get_location_by_id($location_id);
		$data['query']=$this->mstock->get_items_by_location($location_id,"yes",true);
		$this->load->view('wh_include/items_in_acc',$data);	
	}
	
	function items_in_product($product_id,$location_id)
	{
		$data['rlocation']=$this->mstock->get_location_by_id($location_id);
		$data['rproduct']=$this->mstock->get_product_by_id($product_id);
		$data['query']=$this->mstock->get_items_by_product($product_id,$location_id,'yes');
		$this->load->view('wh_include/items_in_product',$data);	
	}
	
	function items_in_product_acc($product_id,$location_id)
	{
		$data['rlocation']=$this->mstock->get_location_by_id($location_id);
		$data['rproduct']=$this->mstock->get_product_by_id($product_id);
		$data['query']=$this->mstock->get_items_by_product($product_id,$location_id,'yes');
		$this->load->view('wh_include/items_in_product_acc',$data);	
	}
	
	function view_items($items_id)
	{
		$data['qact']=$this->mstock->get_activity_by_items($items_id);
		$data['row']=$this->mstock->get_items_by_id($items_id);
		$this->load->view('wh_include/view_items',$data);	
	}
	
	function edit_items($items_id)
	{
		$data['row']=$this->mstock->get_items_by_id($items_id);
		$this->load->view('wh_include/edit_items',$data);	
	}
	
	function stock_opname()
	{
		$this->load->view('wh_include/stock_opname');	
	}
	
	function stock_opname2()
	{
		$this->load->view('wh_include/stock_opname2');	
	}
	
	function stock_opname4()
	{
		$this->load->view('wh_include/stock_opname4');	
	}
	
	function stock_o_result($items_code,$is_second,$is_modem)
	{
		$ada=$this->mstock->check_ada($items_code,$is_second,$is_modem);
		
		if($ada>0)
		{
			$row=$this->mstock->get_items_by_code($items_code);
			//echo $this->db->last_query();
			$this->mstock->items_id=$row->items_id;
			$this->mstock->stock_check_in();
			$data['row']=$this->mstock->get_items_by_code($items_code);
		}
		$data['ada']=$ada;
		$query=$this->mstock->get_stock_check($this->session->userdata('user_id'),date('dmy'),$is_second,$is_modem);
		$data['query']=$query->result();
		$data['totalnya']=$query->num_rows;
		$data['is_second']=$is_second;
		$data['is_modem']=$is_modem;
		$this->load->view('wh_include/stock_o_result',$data);	
	}
	
	function not_found($is_second=0,$is_modem=0)
	{
		$data['query']=$this->mstock->compare_stock($is_second,$is_modem);
		$data['is_second']=$is_second;
		$data['is_modem']=$is_modem;
		$this->load->view('wh_include/not_found',$data);	
	}
	
	function not_found_by_date($date,$is_second=0,$is_modem=0)
	{
		$data['query']=$this->mstock->compare_stock_by_date($date,$is_second,$is_modem);
		$data['is_second']=$is_second;
		$data['is_modem']=$is_modem;
		$this->load->view('wh_include/not_found',$data);
	}
	
	function not_found_xls($is_second=0,$is_modem=0)
	{
		$data['query']=$this->mstock->compare_stock($is_second,$is_modem);
		$table=$this->load->view('wh_include/not_found_xls',$data);	
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=notfound_stockopname_".date('dmy').".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $table;	
	}
	
	function print_stock_opname()
	{
		$query=$this->mstock->get_stock_check($this->session->userdata('user_id'),date('dmy'));
		$data['query']=$query->result();
		$data['totalnya']=$query->num_rows;
		$table=$this->load->view('wh_include/so_print',$data);
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=stockopname_".date('dmy').".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $table;
	}
	
	function add_new_stock($items_id)
	{
		$data['row']=$this->mstock->get_items_by_id($items_id);
		$this->load->view('wh_include/add_new_stock',$data);	
	}
	
	function save_add_stock()
	{
		$new_stock=$this->input->post('items_qty')+$this->input->post('new_qty');
		$this->mstock->update_stock($this->input->post('items_id'),$new_stock);	
	}

	function stock_management()
	{
		$this->load->view('wh_include/stock_management');
	}

	function generate_list_result()
	{
		$query=$this->mstock->get_items_by_filter($this->input->post('is_second'),$this->input->post('product_id'));
		$data['query']=$query;
		$this->load->view('wh_include/generate_list_result',$data);
	}
}
