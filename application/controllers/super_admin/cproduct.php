<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cproduct extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			redirect('program');
    }
	
	function index()
	{
		$data['qcategory']=$this->mmasterdata->get_all_category();
		$data['query']=$this->mmasterdata->get_all_product();
		$this->load->view('admin_include/product',$data);
	}
	
	function add_new()
	{
		$check_exist=$this->mmasterdata->check_product_exist($this->input->post('product_name'));;
		if($check_exist==0)
		{
			$this->mmasterdata->product_name=$this->input->post('product_name');
			$this->mmasterdata->category_id=$this->input->post('category_id');
			$this->mmasterdata->product_information=$this->input->post('product_information');
			$this->mmasterdata->product_specification=nl2br($this->input->post('product_specification'));
			$this->mmasterdata->add_new_product();
		}
	}
	
	function add_new2()
	{
		$check_exist=$this->mmasterdata->check_product_exist($this->input->post('product_name'));;
		if($check_exist==0)
		{
			$this->mmasterdata->product_name=$this->input->post('product_name');
			$this->mmasterdata->category_id=$this->input->post('category_id');
			$this->mmasterdata->product_information=$this->input->post('product_information');
			$this->mmasterdata->product_specification=nl2br($this->input->post('product_specification'));
			$this->mmasterdata->add_new_product();
		}
	}
	
	function edit_now()
	{
		$this->mmasterdata->product_name=$this->input->post('product_name');
		$this->mmasterdata->category_id=$this->input->post('category_id');
		$this->mmasterdata->product_information=$this->input->post('product_information');
		$this->mmasterdata->product_specification=nl2br($this->input->post('product_specification'));
		$this->mmasterdata->edit_product($this->input->post('product_id'));
	}
	
	function edit($product_id)
	{
		$data['qcategory']=$this->mmasterdata->get_all_category();
		$data['row']=$this->mmasterdata->get_product_by_id($product_id);
		$this->load->view('admin_include/edit_product',$data);
	}
	
	function load_spec($product_id)
	{
		$row=$this->mmasterdata->load_spec($product_id);
		echo $row->product_specification;	
	}
	
	function type_acc()
	{
		$data['qcategory']=$this->mmasterdata->get_all_category(true);
		$data['query']=$this->mmasterdata->get_all_product(true);
		$this->load->view('admin_include/product2',$data);
	}
	
	
}