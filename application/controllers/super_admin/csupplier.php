<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csupplier extends CI_Controller {
	
	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			redirect('program');
    }
	
	function index()
	{
		$data['query']=$this->mmasterdata->get_all_supplier();
		$this->load->view('admin_include/supplier',$data);	
	}
	
	function add_new()
	{
		$this->mmasterdata->supplier_name=$this->input->post('supplier_name');
		$this->mmasterdata->supplier_address=$this->input->post('supplier_address');
		$this->mmasterdata->supplier_phone=$this->input->post('supplier_phone');
		$this->mmasterdata->supplier_cp=$this->input->post('supplier_cp');
		$this->mmasterdata->supplier_city=$this->input->post('supplier_city');
		$this->mmasterdata->supplier_email=$this->input->post('supplier_email');
		$this->mmasterdata->supplier_website=$this->input->post('supplier_website');
		$this->mmasterdata->supplier_account=$this->input->post('supplier_account');
		$this->mmasterdata->add_new_supplier();	
	}
}