<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccategory extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			$this->load->view('login');
    }

	public function index()
	{
		$data['query']=$this->mmasterdata->get_all_category();
		$this->load->view('admin_include/category',$data);	
	}
	
	function add_new()
	{
		$this->mmasterdata->category_name=$this->input->post('category_name');
		$this->mmasterdata->category_detail=$this->input->post('category_detail');
		$this->mmasterdata->add_new_category();
	}
	
	function category_acc()
	{
		$data['query']=$this->mmasterdata->get_all_category(true);
		$this->load->view('admin_include/category_acc',$data);
	}
}