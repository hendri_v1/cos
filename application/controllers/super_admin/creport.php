<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creport extends CI_Controller {
	
	function sales_report()
	{
		$this->load->view('report_include/sales_report');	
	}
	
	function generate_sales_report()
	{
		$start_date=human_to_unix($this->input->post('start_date').' 00:00:00');
		$end_date=human_to_unix($this->input->post('end_date').' 23:59:59');	
		$data['query']=$this->mselling->generate_report_per_sales($start_date,$end_date);
		$data['start_date']=$start_date;
		$data['end_date']=$end_date;
		$returnnya=$this->load->view('report_include/generated_report_per_sales',$data,true);
		echo $returnnya;
	}

	function per_sales_detail()
	{
		$start_date=$this->input->post('start_date');
		$end_date=$this->input->post('end_date');
		$data['query']=$this->mselling->per_user_detail_sell($start_date,$end_date,$this->input->post('user_id'));
		$returnnya=$this->load->view('report_include/per_user_detail_sell',$data,true);
		echo $returnnya;
	}

	function sold_out()
	{
		$data['query']=$this->mstock->get_items_by_location2();
		$this->load->view('report_include/sold_out',$data);
	}
	
	function stock_in_report()
	{
		$this->load->view('report_include/stock_in_report');
	}

	function generate_stock_in_report($supplier_id=0)
	{
		$start_date=human_to_unix($this->input->post('start_date').' 00:00:00');
		$end_date=human_to_unix($this->input->post('end_date').' 23:59:59');
		if($supplier_id==0)
		{
			$data['query']=$this->mstock->get_items_date_in_range($start_date,$end_date);
			
		}
		else
		{
			$data['query']=$this->mstock->get_items_date_in_range_per_sup($start_date,$end_date,$supplier_id);
			$data['query2']=$this->mstock->get_items_date_in_range($start_date,$end_date);
		}
		$data['supplier_idx']=$supplier_id;
		$returnnya=$this->load->view('report_include/generated_report_stock_in',$data,true);
		echo $returnnya;
	}

	function detail_items_in()
	{
		$data['query']=$this->mstock->get_items_in_by_ref($this->input->post('items_source_ref'));
		$returnnya=$this->load->view('report_include/items_in_detail',$data,true);
		echo $returnnya;
	}
	
	function gl_report()
	{
		$this->load->view('report_include/gl_report');	
	}
	
	function generate_gl_report()
	{
		$start_date=human_to_unix($this->input->post('start_date').' 00:00:00');
		$end_date=human_to_unix($this->input->post('end_date').' 23:59:59');
		$data['start_date']=$start_date;
		$data['end_date']=$end_date;
		$data['query']=$this->mgl->get_gl_range($start_date,$end_date);
		$data['start_date']=$start_date;
		$this->load->view('report_include/gl_report_result',$data);	
	}

	function send_gl_email()
	{
		$start_date=$this->input->post('start_date');
		$end_date=$this->input->post('end_date');
		$data['start_date']=$start_date;
		$data['end_date']=$end_date;
		$data['query']=$this->mgl->get_gl_range($start_date,$end_date);
		$data['start_date']=$start_date;
		$content=$this->load->view('report_include/gl_report_email',$data,true);	
		$ci = get_instance();
		$ci->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "mitra70674@gmail.com"; 
		$config['smtp_pass'] = "jogja12345";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$ci->email->initialize($config);

		$ci->email->from('mitra70674@gmail.com', 'Mitra Phonsel');
		$list = array('raflesiacel@yahoo.com');
		$ci->email->to($list);
		$this->email->reply_to('mitra70674@gmail.com', 'Mitra Phonsel');
		$ci->email->subject('GL Report');
		$ci->email->message($content);
		$ci->email->send();
	}
	
	function generate_gl_today()
	{
		$start_date=human_to_unix(date('Y-m-d 00:00:00'));
		$end_date=human_to_unix(date('Y-m-d 23:59:59'));
		$data['query']=$this->mgl->get_gl_today($start_date,$end_date);
		$this->load->view('report_include/gl_report_today',$data);	
	}
	
	function so_report()
	{
		$this->load->view('report_include/so_report');	
	}
	
	function generate_so_report()
	{
		$start_date=human_to_unix($this->input->post('start_date').' 00:00:00');
		$end_date=human_to_unix($this->input->post('end_date').' 23:59:59');
		$data['start_date']=$start_date;
		$data['end_date']=$end_date;
		$data['query']=$this->mselling->get_sell_out_range($start_date,$end_date);
		$this->load->view('report_include/so_report_result',$data);
	}

	function send_so_email()
	{
		$start_date=$this->input->post('start_date');
		$end_date=$this->input->post('end_date');
		$data['start_date']=$start_date;
		$data['end_date']=$end_date;
		$data['query']=$this->mselling->get_sell_out_range($start_date,$end_date);
		$content=$this->load->view('report_include/so_report_email',$data,true);
		$ci = get_instance();
		$ci->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://smtp.gmail.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "mitra70674@gmail.com"; 
		$config['smtp_pass'] = "jogja12345";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$ci->email->initialize($config);

		$ci->email->from('mitra70674@gmail.com', 'Mitra Phonsel');
		$list = array('raflesiacel@yahoo.com');
		$ci->email->to($list);
		$this->email->reply_to('mitra70674@gmail.com', 'Mitra Phonsel');
		$ci->email->subject('Sales Report');
		$ci->email->message($content);
		$ci->email->send();
	}
	
	function retur_list()
	{
		$data['query']=$this->mstock->get_items_by_status(2);
		$this->load->view('report_include/retur_list',$data);	
	}
	
	function in_supplier()
	{
		$data['query']=$this->mstock->get_items_by_status(3);
		$this->load->view('report_include/in_supplier',$data);	
	}
	
	function print_retur($items_id)
	{
		$data['query']=$this->mstock->change_items_status($items_id,3);
		$data['row']=$this->mstock->get_items_by_id($items_id);
		$this->load->view('report_include/print_return',$data);
	}
	
	function retur_record_add()
	{
		$this->db->set('items_id',$this->input->post('items_id'));
		$this->db->set('retur_note',$this->input->post('retur_note'));
		$this->db->set('retur_date',time());
		$this->db->set('user_id',$this->input->post('user_id'));
		$this->db->insert('retur_record');	
	}

	function save_return_money()
	{
		$general_ledger_balance=$this->mgl->get_last_balance();
		$this->mgl->general_ledger_title=$this->input->post('gl_title');
		$this->mgl->general_ledger_type=0;
		$this->mgl->general_ledger_total=$this->input->post('gl_total');
		$this->mgl->general_ledger_balance=$general_ledger_balance+$this->input->post('gl_total');
		$this->mgl->general_ledger_ref='none';
		$this->mgl->add_gl();

		$this->mstock->change_items_status($this->input->post('items_id'),5);

	}

	function unfinished_sales()
	{
		$data['query']=$this->mselling->get_lock_nota();
		$this->load->view('report_include/unfinished_sales',$data);
	}
}