<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmain extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('logged') == FALSE)
			redirect('program');
    }
	
	function search_result()
	{
		$keyword=$this->input->post('keyword');
		$pisah=explode(' - ',$keyword);
		$product_id=$pisah[0];
		
		$data['rproduct']=$this->mstock->get_product_by_id($product_id);
		$data['query']=$this->mstock->get_items_by_product($product_id);
		$return=$this->load->view('global_include/search_result',$data,true);
		echo $return;
	}
	
	function search_result2()
	{
		$keyword=$this->input->post('keyword');
		$data['keyword']=$keyword;
		$data['query']=$this->mmasterdata->search_product($keyword);
		$return=$this->load->view('global_include/search_result2',$data,true);
		echo $return;	
	}
	
	function search_result3($the_price)
	{
		$data['query']=$this->mstock->get_items_by_price($the_price);
		$this->load->view('global_include/search_result3',$data);	
	}
	
	function new_sell_out()
	{
		$data['new_id']=$this->mselling->get_last_id();
		$kembali=$this->mstock->get_items_for_json();
		$data['the_items_code']=json_encode($kembali['items_code']);
		$this->load->view('global_include/sell_out',$data);	
	}
	
	function get_items()
	{
		$row=$this->mstock->get_items_by_code($this->input->post('items_code'));
		$kembali=array();
		$kembali['items_name']=$row->category_name.' '.$row->product_name.' '.$row->items_name;
		$kembali['items_base_price']=$row->items_base_price;
		$kembali['items_sell_price']=$row->items_sell_price;
		$kembali['items_id']=$row->items_id;
		$kembali['items_unique']=$row->items_unique;
		$Kembali['items_qty']=$row->items_qty;
		echo json_encode($kembali);	
	}
	
	function save_new_sell()
	{
		$this->mselling->customer_name=$this->input->post('customer_name');
		$this->mselling->customer_phone=$this->input->post('customer_phone');
		$this->mselling->customer_address=$this->input->post('customer_address');
		$this->mselling->sell_out_type=$this->input->post('sell_out_type');
		$this->mselling->sell_out_id=$this->input->post('sell_out_id');
		$this->mselling->sell_out_total=$this->input->post('sell_out_total');
		$this->mselling->user_id=$this->input->post('user_id');
		$this->mselling->save_sell_out();
		
		$dquery=$this->mselling->get_detail_by_id($this->input->post('sell_out_id'));
		/*foreach($dquery as $rows)
		{
			$general_ledger_balance=$this->mgl->get_last_balance();
			$this->mgl->general_ledger_type=0;
			$this->mgl->general_ledger_title='Handphone Sale';
			$this->mgl->general_ledger_total=$rows->detail_sell_out_total;
			$this->mgl->general_ledger_balance=$general_ledger_balance+$rows->detail_sell_out_total;
			$this->mgl->general_ledger_ref=$rows->detail_sell_out_id;
			$this->mgl->add_gl();	
		}*/
	}
	
	function save_detail_sell_out()
	{
		if($this->input->post('items_unique')==0)
		{
			$this->mselling->items_id=$this->input->post('items_id');
			$this->mselling->detail_sell_out_price=$this->input->post('detail_sell_out_price');
			$this->mselling->qty=$this->input->post('qty');
			$this->mselling->sell_out_id=$this->input->post('sell_out_id');
			$detail_sell_out_total=$this->input->post('qty')*$this->input->post('detail_sell_out_price');
			$this->mselling->detail_sell_out_total=$detail_sell_out_total;
			$this->mselling->save_detail_sell_out();
		}
		else
		{
			$this->mselling->items_id=$this->input->post('items_id');
			$this->mselling->detail_sell_out_price=$this->input->post('detail_sell_out_price');
			$this->mselling->qty=$this->input->post('qty');
			$this->mselling->sell_out_id=$this->input->post('sell_out_id');
			$detail_sell_out_total=$this->input->post('qty')*$this->input->post('detail_sell_out_price');
			$this->mselling->detail_sell_out_total=$detail_sell_out_total;
			$this->mselling->save_detail_sell_out2($this->input->post('items_qty'));
		}
		//save the log
		$this->mstock->add_activity($this->input->post('location_id'),$this->input->post('items_id'),$this->session->userdata('user_id'),2,'Sell With Nota No '.$this->input->post('sell_out_id'));
	}
	
	function remove_detail_sell_out()
	{
		$this->mselling->items_id=$this->input->post('items_id');
		$this->mselling->remove_detail_sell_out($this->input->post('detail_sell_out_id'));	
	}
	
	function remove_detail_sell_out2()
	{
		$row=$this->mselling->get_detail_by_dsell_out_id($this->input->post('detail_sell_out_id'));
		$this->mgl->general_ledger_title=$row->sell_out_id.' (cancelled) '.$row->category_name.' '.$row->product_name.' '.$row->items_name;
		$this->mgl->update_gl_by_general_ledger_ref($this->input->post('detail_sell_out_id'));
		
		$this->mselling->items_id=$this->input->post('items_id');
		$this->mselling->return_detail_sell_out($this->input->post('detail_sell_out_id'));
		
	}
	
	function cancel_sell()
	{
		$this->mselling->cancel_sell($this->input->post('sell_out_id'));
	}
	
	function refresh_detail($sell_out_id)
	{
		$data['query']=$this->mselling->get_detail_by_id($sell_out_id);
		$this->load->view('global_include/detail_sell_out',$data);	
	}
	
	function unpaid_sales()
	{
		$data['kembali']=$this->mselling->get_all_sell_json();
		$data['query']=$this->mselling->get_all_unpaid();
		
		$kembali=$this->mstock->get_items_for_json_all();
		$data['the_items_code']=json_encode($kembali['items_code']);
		
		$this->load->view('global_include/unpaid_sales',$data);
	}
	
	function ubah_nota()
	{
		$this->mselling->change_nota();
	}
	
	function sales_info($sell_out_id)
	{
		$data['row']=$this->mselling->get_sell_out_by_id($sell_out_id);
		$data['dquery']=$this->mselling->get_detail_by_id($sell_out_id);
		$this->load->view('global_include/sales_info',$data);
	}
	
	function print_sell_out($sell_out_id)
	{
		$data['row']=$this->mselling->get_sell_out_by_id($sell_out_id);
		$data['dquery']=$this->mselling->get_detail_by_id($sell_out_id);
		$this->mselling->update_sell_out_by_id($sell_out_id);
		$this->load->view('global_include/print_sell_out',$data);
	}
	
	function view_items_by_code($items_code)
	{
		$row1=$this->mstock->get_items_by_code_all($items_code);
		$items_id=$row1->items_id;
		$rdetail=$this->mselling->get_detail_by_items_id($items_id);
		$data['row']=$this->mselling->get_sell_out_by_id($rdetail->sell_out_id);
		$data['dquery']=$this->mselling->get_detail_by_id($rdetail->sell_out_id);
		$this->load->view('global_include/sales_info',$data);
	}
	
	function return_sell()
	{
		$this->load->view('global_include/return_sell');
	}
	
	function sales_info2($sell_out_id)
	{
		$data['row']=$this->mselling->get_sell_out_by_id($sell_out_id);
		$data['dquery']=$this->mselling->get_detail_by_id($sell_out_id);
		$this->load->view('global_include/sales_info2',$data);
	}
	
	function general_ledger()
	{
		$this->load->view('global_include/general_ledger');	
	}
	
	function add_gl()
	{
		$general_ledger_balance=$this->mgl->get_last_balance();
		$this->mgl->general_ledger_type=$this->input->post('general_ledger_type');
		$this->mgl->general_ledger_title=$this->input->post('general_ledger_title');
		$this->mgl->general_ledger_total=$this->input->post('general_ledger_total');
		if($this->input->post('general_ledger_type')==0)
			$this->mgl->general_ledger_balance=$general_ledger_balance+$this->input->post('general_ledger_total');
		else
			$this->mgl->general_ledger_balance=$general_ledger_balance-$this->input->post('general_ledger_total');
		$this->mgl->general_ledger_ref='none';
		$this->mgl->add_gl();
	}
}